package gerber.apress.com.reminders2e;


import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class ReminderRepository {

    private ReminderDao mReminerDao;
    private LiveData<List<Reminder>> mAllReminders;

    ReminderRepository(Application application) {
        RemindersRoomDatabase db = RemindersRoomDatabase.getDatabase(application);
        mReminerDao = db.wordDao();
        mAllReminders = mReminerDao.getAllWords();
    }

    LiveData<List<Reminder>> getAllReminders() {
        return mAllReminders;
    }


    public void insert (Reminder word) {
        new insertAsyncTask(mReminerDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<Reminder, Void, Void> {

        private ReminderDao mAsyncTaskDao;

        insertAsyncTask(ReminderDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Reminder... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}