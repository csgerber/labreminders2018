package gerber.apress.com.reminders2e;

import android.arch.persistence.room.RoomDatabase;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {Reminder.class}, version = 1)
public abstract class RemindersRoomDatabase extends RoomDatabase {

    public abstract ReminderDao wordDao();

    private static volatile RemindersRoomDatabase INSTANCE;

    static RemindersRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RemindersRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RemindersRoomDatabase.class, "reminders")
                           // .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final ReminderDao mDao;

        PopulateDbAsync(RemindersRoomDatabase db) {
            mDao = db.wordDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            mDao.deleteAll();


//            Reminder word = new Reminder("Hello");
//            mDao.insert(word);
//            word = new Reminder("World");
//            mDao.insert(word);
            return null;
        }
    }
}