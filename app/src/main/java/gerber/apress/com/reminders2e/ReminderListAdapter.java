package gerber.apress.com.reminders2e;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ReminderListAdapter extends RecyclerView.Adapter<ReminderListAdapter.ReminderViewHolder> {
    private Context context;
    class ReminderViewHolder extends RecyclerView.ViewHolder {
        private final TextView rowText;
        private final View rowTab;



        private ReminderViewHolder(View itemView) {
            super(itemView);
            rowText = itemView.findViewById(R.id.row_text);
            rowTab = itemView.findViewById(R.id.row_tab);
        }


        public TextView getRowText() {
            return rowText;
        }

        public View getRowTab() {
            return rowTab;
        }


    }

    private final LayoutInflater mInflater;
    private List<Reminder> mReminders; // Cached copy of words

    ReminderListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;

    }

    @Override
    public ReminderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = mInflater.inflate(R.layout.reminders_row, parent, false);
        return new ReminderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReminderViewHolder holder, int position) {

        Reminder current = mReminders.get(position);
        if (mReminders != null) {

            holder.rowText.setText(current.getContent());
        } else {
            // Covers the case of data not being ready yet.
            holder.rowText.setText("No Reminder");
        }

        //put the logic to assign the color here.
    if (current.getImportant() > 0) {
        holder.rowTab.setBackgroundColor(   context.getResources().getColor(R.color.orange));
    } else {
        holder.rowTab.setBackgroundColor(context.getResources().getColor(R.color.green));
    }





    }

    void setReminders(List<Reminder> words){
        mReminders = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mReminders has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mReminders != null)
            return mReminders.size();
        else return 0;
    }
}