package gerber.apress.com.reminders2e;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ReminderDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Reminder reminder);

    @Query("DELETE FROM reminders")
    void deleteAll();

    @Query("SELECT * from reminders ORDER BY content ASC")
    LiveData<List<Reminder>> getAllWords();
}